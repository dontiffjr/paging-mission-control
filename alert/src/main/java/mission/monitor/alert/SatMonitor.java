package mission.monitor.alert;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * Paging Mission Control

You are tasked with assisting satellite ground operations for an earth science mission that monitors magnetic field 
variations at the Earth's poles. A pair of satellites fly in tandem orbit such that at least one will have line of 
sight with a pole to take accurate readings. The satellite’s science instruments are sensitive to changes in temperature 
and must be monitored closely. Onboard thermostats take several temperature readings every minute to ensure that the 
precision magnetometers do not overheat. Battery systems voltage levels are also monitored to ensure that power is 
available to cooling coils. Design a monitoring and alert application that processes status telemetry from the satellites 
and generates alert messages in cases of certain limit violation scenarios.


Requirements
Ingest status telemetry data and create alert messages for the following violation conditions:

If for the same satellite there are three battery voltage readings that are under the red low limit within a five minute interval.
If for the same satellite there are three thermostat readings that exceed the red high limit within a five minute interval.


Input Format
The program is to accept a file as input. The file is an ASCII text file containing pipe delimited records.
The ingest of status telemetry data has the format:
<timestamp>|<satellite-id>|<red-high-limit>|<yellow-high-limit>|<yellow-low-limit>|<red-low-limit>|<raw-value>|<component>
You may assume that the input files are correctly formatted. Error handling for invalid input files may be ommitted.

Output Format
The output will specify alert messages.  The alert messages should be valid JSON with the following properties:
{
    "satelliteId": 1234,
    "severity": "severity",
    "component": "component",
    "timestamp": "timestamp"
}
The program will output to screen or console (and not to a file).

Sample Data
The following may be used as sample input and output datasets.

Input
20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT
20180101 23:01:09.521|1000|17|15|9|8|7.8|BATT
20180101 23:01:26.011|1001|101|98|25|20|99.8|TSTAT
20180101 23:01:38.001|1000|101|98|25|20|102.9|TSTAT
20180101 23:01:49.021|1000|101|98|25|20|87.9|TSTAT
20180101 23:02:09.014|1001|101|98|25|20|89.3|TSTAT
20180101 23:02:10.021|1001|101|98|25|20|89.4|TSTAT
20180101 23:02:11.302|1000|17|15|9|8|7.7|BATT
20180101 23:03:03.008|1000|101|98|25|20|102.7|TSTAT
20180101 23:03:05.009|1000|101|98|25|20|101.2|TSTAT
20180101 23:04:06.017|1001|101|98|25|20|89.9|TSTAT
20180101 23:04:11.531|1000|17|15|9|8|7.9|BATT
20180101 23:05:05.021|1001|101|98|25|20|89.9|TSTAT
20180101 23:05:07.421|1001|17|15|9|8|7.9|BATT

Ouput
[
    {
        "satelliteId": 1000,
        "severity": "RED HIGH",
        "component": "TSTAT",
        "timestamp": "2018-01-01T23:01:38.001Z"
    },
    {
        "satelliteId": 1000,
        "severity": "RED LOW",
        "component": "BATT",
        "timestamp": "2018-01-01T23:01:09.521Z"
    }
]
 *
 */
public class SatMonitor 
{
	private static int timestamp_position = 0;
	private static int satellite_id_position = 1;
	private static int red_high_limit_position = 2;
	private static int yellow_high_limit_position = 3;
	private static int yellow_low_limit_position = 4;
	private static int red_low_limit_position = 5;
	private static int raw_value_position = 6;
	private static int component_position = 7;
	private static String message_delim = "[\\|]";
	private static String battery = "BATT";
	private static String thermostat = "TSTAT";
	private static long battery_alert_time_interval_ms = 300000;
	private static long thermostat_alert_time_interval_ms = 300000;
	
	private static HashMap<String,ArrayList<Timestamp>> lowVoltageBySatId = new HashMap<String,ArrayList<Timestamp>>();
	private static ArrayList<Alert> battertyAlerts = new ArrayList<Alert>();
	
	private static HashMap<String,ArrayList<Timestamp>> highTempBySatId = new HashMap<String,ArrayList<Timestamp>>();
	private static ArrayList<Alert> tempAlerts = new ArrayList<Alert>();
	
    public static void main( String[] args )
    {
    	if(args != null && args[0] != null) {
    		BufferedReader readIn;
    		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");
    		try {
    			readIn = new BufferedReader(new FileReader(args[0]));
    			String str = readIn.readLine();
    			while (str != null) {
    				
    				ingestStatusTelemetryData(str, dateFormat);
    				str = readIn.readLine();
    			}
    			readIn.close();
    			System.out.println("[");
    			
    			if(!SatMonitor.tempAlerts.isEmpty()) {
    				// Use old style counter so I know when on last element
					for(int a = 0;a < SatMonitor.tempAlerts.size();a++) {
						System.out.println("{");
						System.out.println("\"satelliteId\": " + SatMonitor.tempAlerts.get(a).getSatelliteId() + " ,");
						System.out.println("\"severity\": \"" + SatMonitor.tempAlerts.get(a).getSeverity() + "\" ,");
						System.out.println("\"component\": \"" + SatMonitor.tempAlerts.get(a).getComponent() + "\" ,");
						System.out.println("\"timestamp\": \"" + SatMonitor.tempAlerts.get(a).getTimestamp().toString() + "Z\"");
						if(a == SatMonitor.tempAlerts.size() - 1) {
							System.out.println("}");
						} else {
							System.out.println("},");
						}
					}
				}
    			if(!SatMonitor.tempAlerts.isEmpty()) {
    			    System.out.println(",");
    			}
    			if(!SatMonitor.battertyAlerts.isEmpty()) {
    				// Use old style counter so I know when on last element
					for(int a =0;a < SatMonitor.battertyAlerts.size();a++) {
						System.out.println("{");
						System.out.println("\"satelliteId\": " + SatMonitor.battertyAlerts.get(a).getSatelliteId() + " ,");
						System.out.println("\"severity\": \"" + SatMonitor.battertyAlerts.get(a).getSeverity() + "\" ,");
						System.out.println("\"component\": \"" + SatMonitor.battertyAlerts.get(a).getComponent() + "\" ,");
						System.out.println("\"timestamp\": \"" + SatMonitor.battertyAlerts.get(a).getTimestamp().toString() + "Z\"");
						if(a == SatMonitor.battertyAlerts.size() - 1) {
							System.out.println("}");
						} else {
							System.out.println("},");
						}
					}
				}
    			
    			System.out.println("]");
    			
    		} catch (Exception e) {
    			
    			System.out.println( "Abort: Error occurred while trying to read input file specified!" );
    			e.printStackTrace();
    		}
				
    	} else {
    		System.out.println( "Abort: Must specify path to input file!" );
    	}
    }
    
    /**
     * Process one telemetry line. Output alerts if:
     * for the same satellite there are three battery voltage readings that are under the red low limit within a five minute interval.
     * for the same satellite there are three thermostat readings that exceed the red high limit within a five minute interval.
     * @param data
     */
    private static void ingestStatusTelemetryData(String data, SimpleDateFormat dateFormat) {
    	
    	String[] dataParts = data.split(SatMonitor.message_delim);
    	Date dateRecorded = null;
    	Timestamp timeStamp = null;
    	
		try {
			dateRecorded = dateFormat.parse(dataParts[SatMonitor.timestamp_position]);
			timeStamp = new Timestamp(dateRecorded.getTime());
			
			if(SatMonitor.battery.equals(dataParts[SatMonitor.component_position])) {
				if(Double.valueOf(dataParts[SatMonitor.raw_value_position]).doubleValue() < 
						Integer.valueOf(dataParts[SatMonitor.red_low_limit_position]).doubleValue()) {
					if(lowVoltageBySatId.containsKey(dataParts[SatMonitor.satellite_id_position])) {
						
						Timestamp alertLimit = new Timestamp(timeStamp.getTime() - SatMonitor.battery_alert_time_interval_ms);
						
						while(!lowVoltageBySatId.get(dataParts[SatMonitor.satellite_id_position]).isEmpty() && 
							   lowVoltageBySatId.get(dataParts[SatMonitor.satellite_id_position]).get(0).before(alertLimit)   ) {
							lowVoltageBySatId.get(dataParts[SatMonitor.satellite_id_position]).remove(0);
						}
						lowVoltageBySatId.get(dataParts[SatMonitor.satellite_id_position]).add(timeStamp);
						if(lowVoltageBySatId.get(dataParts[SatMonitor.satellite_id_position]).size() > 2) {
							Alert alert = new Alert();
							alert.setComponent(SatMonitor.battery);
							alert.setSatelliteId(dataParts[SatMonitor.satellite_id_position]);
							alert.setSeverity("RED LOW");
							alert.setTimestamp(lowVoltageBySatId.get(dataParts[SatMonitor.satellite_id_position]).get(0));
							SatMonitor.battertyAlerts.add(alert);
						}
					} else {
						lowVoltageBySatId.put(dataParts[SatMonitor.satellite_id_position], new ArrayList<Timestamp>());
						lowVoltageBySatId.get(dataParts[SatMonitor.satellite_id_position]).add(timeStamp);
					}
				}
				
				
			} else if(SatMonitor.thermostat.equals(dataParts[SatMonitor.component_position])) {
				if(Double.valueOf(dataParts[SatMonitor.raw_value_position]).doubleValue() > 
						Integer.valueOf(dataParts[SatMonitor.red_high_limit_position]).doubleValue()) {
					if( highTempBySatId.containsKey(dataParts[SatMonitor.satellite_id_position])) {
						
						Timestamp alertLimit = new Timestamp(timeStamp.getTime() - SatMonitor.thermostat_alert_time_interval_ms);
						
						while(! highTempBySatId.get(dataParts[SatMonitor.satellite_id_position]).isEmpty() && 
								 highTempBySatId.get(dataParts[SatMonitor.satellite_id_position]).get(0).before(alertLimit)   ) {
							 highTempBySatId.get(dataParts[SatMonitor.satellite_id_position]).remove(0);
						}
						 highTempBySatId.get(dataParts[SatMonitor.satellite_id_position]).add(timeStamp);
						if( highTempBySatId.get(dataParts[SatMonitor.satellite_id_position]).size() > 2) {
							Alert alert = new Alert();
							alert.setComponent(SatMonitor.thermostat);
							alert.setSatelliteId(dataParts[SatMonitor.satellite_id_position]);
							alert.setSeverity("RED HIGH");
							alert.setTimestamp( highTempBySatId.get(dataParts[SatMonitor.satellite_id_position]).get(0));
							SatMonitor.tempAlerts.add(alert);
						}
					} else {
						 highTempBySatId.put(dataParts[SatMonitor.satellite_id_position], new ArrayList<Timestamp>());
						 highTempBySatId.get(dataParts[SatMonitor.satellite_id_position]).add(timeStamp);
					}
				}
				
			}
	    	
	    	
	    	
		} catch (ParseException e) {
			
			e.printStackTrace();
		}

    	System.out.println(data);
    }
}
