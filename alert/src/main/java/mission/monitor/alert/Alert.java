/**
 * 
 */
package mission.monitor.alert;

import java.sql.Timestamp;

/**
 * @author Donald
 *
 */
public class Alert {

    private String satelliteId = null;
    private String severity = null;
    private String component = null;
    private Timestamp timestamp = null;
	/**
	 * @return the satelliteId
	 */
	public String getSatelliteId() {
		return satelliteId;
	}
	/**
	 * @param satelliteId the satelliteId to set
	 */
	public void setSatelliteId(String satelliteId) {
		this.satelliteId = satelliteId;
	}
	/**
	 * @return the severity
	 */
	public String getSeverity() {
		return severity;
	}
	/**
	 * @param severity the severity to set
	 */
	public void setSeverity(String severity) {
		this.severity = severity;
	}
	/**
	 * @return the component
	 */
	public String getComponent() {
		return component;
	}
	/**
	 * @param component the component to set
	 */
	public void setComponent(String component) {
		this.component = component;
	}
	/**
	 * @return the timestamp
	 */
	public Timestamp getTimestamp() {
		return timestamp;
	}
	/**
	 * @param timestamp the timestamp to set
	 */
	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}

}
